# WSP - Wątek Sterowania Procesem (Process Control Thread) #

That repository is for my little code that I use a lot and I always create new one to each project.
So I decided to make 1 that I will use all the time.

I archived that using select, threading and subprocess.

The goal of the code is handle system process (like mongoDB server) and trigger some actions depending of the regex of the output line.
I personaly use it to launch mongoDB server process when I'm crawling the web and I want to close that process after I done.

### How do I get set up? ###

It is realy simple, first you must create WSP object.
On object creation you must add command that it will execute.
It can be string, tuple or list.

Example: 
```
#!python

w = WSP('mongod --dbpath "." --smallfiles')
```
Then add some regexes and function that you want to trigger.

```
#!python
def start(*args):
    print 'it works!'

w.add_event('waiting for connections on port 27017', start)
```
after that you can just do

```
#!python

w.start()
```
and thread will start execute command that you gave him and beginning.
When the output line will match the event regex then it will do event function that you gave him.

The output line (no matter that it is regex one or not) is by default printed using logging.info.
If you want to change it inheritance from WSP and override display method.

You can also trim the output line before that line will be interpreted or even displayed.

Example:

```
#!python
trim = '\d+-\d+-.* \[.*\] (.*)'
w = WSP('mongod --dbpath "." --smallfiles', trim=trim)
```

### Who do I talk to? ###

* Repo owner or admin
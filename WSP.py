#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import re
import threading
import select
import subprocess
import logging

from types import TupleType, ListType, StringType



class WSP(threading.Thread):
    logger = logging.getLogger('WSP')
    running = 1
    event_regexes = []

    def __init__(self, komenda, trim=''):
        self.trim = re.compile(trim) if trim else ''
        self.logger.debug('Got %s' % str(komenda))
        threading.Thread.__init__(self)

        if type(komenda) == StringType:
            self.komenda = komenda.split(' ')

        elif type(komenda) == TupleType or type(komenda) == ListType:
            self.komenda = komenda

        else:
            self.running = 0
            self.logger.error('Commend not String or List/Tuple: %s' % type(komenda))


    def stop(self):
        """Method try to stop process by sending kill -15 to it."""

        try:
            self.proc.terminate()
        except:
            pass


    def salvation(self):
        """Method stops main loop and kills process by sending kill -9 to it."""

        self.running = 0
        try:
            self.proc.kill()
        except:
            pass


    def add_event(self, event_regex, event_func):
        '''1 argument is event regex and 2 argument is a event function that will be called when regex matched.
        Event function always get the regex result.
        Example: add_event('start', lambda *args: print 'ready')

        Hint. if you want to wait until event_regex is found you can add as event_func Queue putting function
        Example: add_event('start', lambda *args: my_queue.put(args))
        then just wait.
        '''

        self.event_regexes.append(
            (
                re.compile(event_regex),
                event_func
            )
        )


    def run(self):
        self.logger.debug('Trying to start')
        try:
            self.proc = subprocess.Popen(
                self.komenda, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE
            )
        except:
            self.logger.error('Can\'t run command', exc_info=True)
            self.running = 0

        self.logger.info('Everyting is ok')

        while self.running:
            iRdy = select.select([self.proc.stdout],[],[], 1)[0]

            if self.proc.stdout in iRdy:
                line = self.proc.stdout.readline()
                line = self.trim_line(line)
                self.interpreter(line)
                self.display(line)

                if self.proc.poll() != None or not line:
                    self.running = 0

        self.logger.debug('Out side main loop')


    def interpreter(self, line):
        """Method check output line. If regex match then triggers event function."""

        for event_regex, event_func in self.event_regexes:
            got_it = event_regex.match(line)

            if got_it:
                event_func(got_it)

    def trim_line(self, line):
        """If trim is setup, method will return group(0) from line.
        Method is called before interpreter method."""

        line = line.strip()
        if self.trim:
            r = self.trim.match(line)
            if r:
                line = r.group(1)

        return line

    def display(self, line):
        """Method displays the output line from process"""

        self.logger.info(line)


if __name__ == '__main__':
    def start_f(*args):
        print '\n\nstart kurwa\n\n'

    logging.basicConfig(level=logging.DEBUG)
    import os
    trim = '\d+-\d+-.* \[.*\] (.*)'
    sr = 'waiting for connections on port 27017'
    p = os.path.dirname(__file__)
    w = WSP('mongod --dbpath %s --smallfiles' % p, trim=trim)
    w.add_event(sr, start_f)
    w.start()
    raw_input()
    w.stop()
    w.join(5)
    if w.isAlive():
        w.salvation()
